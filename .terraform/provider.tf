provider "google" {
  project = "$gcp_project"
  region  = "$region"
  zone    = "$zone"
}
