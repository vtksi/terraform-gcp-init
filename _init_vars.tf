variable "gcp_project" {
  type = string
}
variable "region" {
  type    = string
  default = "us-central1"
}
variable "zone" {
  type    = string
  default = "us-central1-a"
}
variable "enable_create_bucket" {
  description = "If set to true, create bucket and folders"
  type        = bool
  default     = true
}
